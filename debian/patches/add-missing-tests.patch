Description: Add upstream tests.
Author: Abraham Raji <avronr@tuta.io>
Origin: upstream
Forwarded: not-needed
Last-Update: 2020-06-24

--- /dev/null
+++ b/spec/rspec/stubbed_env/test_helpers_spec.rb
@@ -0,0 +1,112 @@
+# frozen_string_literal: true
+
+require 'spec_helper'
+
+RSpec.describe RSpec::StubbedEnv::TestHelpers do
+  describe 'stub_env' do
+    context 'control without stubbing' do
+      subject { FooByEnv.new }
+      it 'is test env' do
+        expect(subject.env).to eq('test')
+      end
+      it 'has 2 localhost settings' do
+        expect(subject.array.grep(/localhost:\d{3}\Z/).length).to eq(2)
+      end
+      it 'has size 2' do
+        expect(subject.size).to eq(2)
+      end
+    end
+    context 'hash signature' do
+      context 'development environment' do
+        include_context 'with stubbed env'
+        let(:environment) { 'development' }
+        before do
+          stub_env(
+              'MEDIUM_1' => 'datastore-01.example.org',
+              'MEDIUM_2' => 'datastore-02.example.org',
+              'IRRELEVANT_3' => 'datastore-03.example.org',
+              )
+          allow(Rails).to receive(:env).and_return(environment)
+        end
+        subject { FooByEnv.new }
+        it 'is development env' do
+          expect(subject.env).to eq(environment)
+        end
+        it 'has 2 example.org settings' do
+          expect(subject.array.grep(/datastore-\d{2}\.example\.org\Z/).length).to eq(2)
+        end
+        it 'has size 2' do
+          expect(subject.size).to eq(2)
+        end
+      end
+      context 'production environment' do
+        include_context 'with stubbed env'
+        let(:environment) { 'production' }
+        before do
+          stub_env(
+              'BIG_1' => 'datastore-01.example.org',
+              'BIG_2' => 'datastore-02.example.org',
+              'BIG_3' => 'datastore-03.example.org',
+              'BIG_4' => 'datastore-04.example.org',
+              'IRRELEVANT_5' => 'datastore-03.example.org',
+              )
+          allow(Rails).to receive(:env).and_return(environment)
+        end
+        subject { FooByEnv.new }
+        it 'is production env' do
+          expect(subject.env).to eq(environment)
+        end
+        it 'has 4 example.org settings' do
+          expect(subject.array.grep(/datastore-\d{2}\.example\.org\Z/).length).to eq(4)
+        end
+        it 'has size 4' do
+          expect(subject.size).to eq(4)
+        end
+      end
+    end
+    context 'key, value signature' do
+      context 'development environment' do
+        include_context 'with stubbed env'
+        let(:environment) { 'development' }
+        before do
+          stub_env('MEDIUM_1', 'datastore-01.example.org')
+          stub_env('MEDIUM_2', 'datastore-02.example.org')
+          stub_env('IRRELEVANT_3', 'datastore-03.example.org')
+          allow(Rails).to receive(:env).and_return(environment)
+        end
+        subject { FooByEnv.new }
+        it 'is development env' do
+          expect(subject.env).to eq(environment)
+        end
+        it 'has 2 example.org settings' do
+          expect(subject.array.grep(/datastore-\d{2}\.example\.org\Z/).length).to eq(2)
+        end
+        it 'has size 2' do
+          expect(subject.size).to eq(2)
+        end
+      end
+      context 'production environment' do
+        include_context 'with stubbed env'
+        let(:environment) { 'production' }
+        before do
+          stub_env('BIG_1', 'datastore-01.example.org')
+          stub_env('BIG_2', 'datastore-02.example.org')
+          stub_env('BIG_3', 'datastore-03.example.org')
+          stub_env('BIG_4', 'datastore-04.example.org')
+          stub_env('IRRELEVANT_5', 'datastore-05.example.org')
+          allow(Rails).to receive(:env).and_return(environment)
+        end
+        subject { FooByEnv.new }
+        it 'is production env' do
+          expect(subject.env).to eq(environment)
+        end
+        it 'has 4 example.org settings' do
+          expect(subject.array.grep(/datastore-\d{2}\.example\.org\Z/).length).to eq(4)
+        end
+        it 'has size 4' do
+          expect(subject.size).to eq(4)
+        end
+      end
+    end
+  end
+end
--- /dev/null
+++ b/spec/rspec/stubbed_env_spec.rb
@@ -0,0 +1,7 @@
+# frozen_string_literal: true
+
+RSpec.describe RSpec::StubbedEnv do
+  it 'has a version number' do
+    expect(RSpec::StubbedEnv::VERSION).not_to be nil
+  end
+end
--- /dev/null
+++ b/spec/spec_helper.rb
@@ -0,0 +1,18 @@
+# frozen_string_literal: true
+
+require 'support/fake_rails'
+require 'support/foo_by_env'
+
+require 'rspec/stubbed_env'
+
+RSpec.configure do |config|
+  # Enable flags like --only-failures and --next-failure
+  config.example_status_persistence_file_path = '.rspec_status'
+
+  # Disable RSpec exposing methods globally on `Module` and `main`
+  config.disable_monkey_patching!
+
+  config.expect_with :rspec do |c|
+    c.syntax = :expect
+  end
+end
--- /dev/null
+++ b/spec/support/fake_rails.rb
@@ -0,0 +1,9 @@
+# frozen_string_literal: true
+
+class Rails
+  class << self
+    def env
+      'test'
+    end
+  end
+end
--- /dev/null
+++ b/spec/support/foo_by_env.rb
@@ -0,0 +1,14 @@
+# frozen_string_literal: true
+
+class FooByEnv
+  attr_reader :array, :size, :env
+  def initialize
+    @env = Rails.env.to_s
+    @array = case @env
+             when 'test' then ['localhost:123', 'localhost:124']
+             when 'development' then [ENV['MEDIUM_1'], ENV['MEDIUM_2']]
+             when 'production' then [ENV['BIG_1'], ENV['BIG_2'], ENV['BIG_3'], ENV['BIG_4']]
+             end.compact
+    @size = @array.length
+  end
+end
